'''
Game Class
Purpose: Creates the Game class for the matching game that allows the user to
input the number of columns and rows of cards and play against the computer.
Author: Kendra Bach
'''
import random
from card import Card
from deck import Deck

class Game(object):
    ''' Creates a subclass of both Card and Deck that simulates gameplay
    between the player and the machine'''

    def __init__(self, rows, columns):
        '''defines instant variables of Game'''
        
        
        self.rows = rows
        self.columns = columns
        self.d = Deck(self.rows, self.columns)
        self.d.shuffle()
        self.gameboard = []
        
        
    
    def displayBoard(self):
        '''Create's the user's view of the Gameboard'''
        for row in self.gameboard:
            for card in row:
                if card.face == 0:
                    print('*', end= " ")
                else:
                    print(card, end= " ")
            print()
    
   
    def populateBoard(self):
        '''Creates display board'''
        self.gameboard = []
        for row in range(self.rows):
            self.gameboard.append([0] * self.columns)
        for row in range(len(self.gameboard)):
            for column in range(len(self.gameboard[row])):
                self.gameboard[row][column] = self.d.deal()
        self.displayBoard()
    
        

    def play(self):
        '''Plays the matching game'''

        self.populateBoard()
        
        total_matches = 0 

        while True:
            while True:
                    card1 = input("Enter coordinates for first card:")
                    card1 = card1.split(' ')
                    card1 = [x for x in card1]
                    card1 = [int(x) for x in card1]
                    card1 = [x-1 for x in card1]
                    if 0 > card1[0] or card1[0] > self.rows or 0 > card1[1] \
                        or card1[1] > self.columns:
                            print("***Invalid coordinates! Try again.***")
                    else:
                        guess_1 = self.gameboard[card1[0]][card1[1]]
                        break
            while True:
                    card2 = input("Enter coordinates for second card:")
                    card2 = card2.split(' ')
                    card2 = [x for x in card2]
                    card2 = [int(x) for x in card2]
                    card2 = [x-1 for x in card2]
                    if 0 > card2[0] or card2[0] > self.rows or 0 \
                          > card2[1] or card2[1] > self.columns:
                        print("***Invalid coordinates! Try again.***")
                    else:
                        guess_2 = self.gameboard[card2[0]][card2[1]]
                        break
                        
            if guess_1 == guess_2 \
                and card2[0] == card1[0] \
                and card2[1] == card1[1]:
                    print("***Same card. Try again.***")
            elif guess_1 == guess_2:
                print("***You found a match!***)")
                guess_1.faceup()
                guess_2.faceup()
                self.displayBoard()
                total_matches += 1
                if total_matches == ((self.rows *self.columns)/2):
                    Game.isGameOver(self)
                    break
            else:
                self.displayBoard()
                print("Not an identical pair. Found " + \
                      str(guess_1) + " at " + "(" + str(card1[1]+1) + ") "\
                      + "and " + str(guess_2) + " at (" + str(card2[1]+1) +")")

 
                          
    def isGameOver(self):
        '''Informs the player that they won and the game is over'''
        print("***Congrats! You found all of the matches! Game over.***")
        
        

        
