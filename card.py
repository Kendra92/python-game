'''
Card Class
Purpose: Creates a Card class that contains each individual card's methods
and attributes.
Author: Kendra Bach
'''

class Card(object):
    '''Creates card class'''
    
    def __init__(self, value):
        '''Assigns instance variables to Card, and self.face = 0
        indicates facedown'''
        self.value = value
        self.face = 0

    def faceup(self):
        '''Changes status of card to faceup'''
        self.face = 1
        

    def __eq__(self, other):
        ''' Compares two cards, self and other, using ==. '''
        if self is other:
            return True
        elif type(self) != type(other): #Distinguishing if the types match
            return False
        else:
            return self.value == other.value

    def __str__(self):
        '''gives string representation of the value of the card'''
        return(str(self.value))
        
    def __ne__(self, other):
        ''' Compares two cards, self and other, using !=. '''
        return not self == other
        
