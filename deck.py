'''
Deck Class
Purpose: Creates the Deck class that contains a 2D list of cards
Author: Kendra Bach
'''
import random
from card import Card

class Deck(object):
    '''Creates a new class, Deck, that contains cards'''

    def __init__(self, rows, columns):
        '''defines instant variables of Deck'''
        self.cards = []
        self.rows = rows
        self.columns = columns

        for num in range(1,(int((self.rows * self.columns)/2) +1)):
            c1 = Card(num)
            self.cards.append(c1)
            c2 = Card(num)
            self.cards.append(c2)        #Appends 2 of the same card 
                                        #to each row and column
    
    def deal(self):
        '''deals the top card or returns None if deck is empty'''
        if len(self) == 0:
            return None
        else:
            return self.cards.pop(0)

    def shuffle(self):
        '''shuffles the deck'''
        random.shuffle(self.cards)
        

    def __len__(self):
        '''returns the number of cards left in the deck'''
        return len(self.cards)

    def __str__(self):
        '''creates a string representation of the deck'''
        self.result = ''
        for c in self.cards:
            self.result = self.result + str(c) + '\n'
        return self.result
        

    

    
